README.txt for Custom Account Link module
---------------------------

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainer

INTRODUCTION
------------

The user module creates a link "My account" to /user in the account menu.
This module lets you override the label with text, or the current user's name.


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 - Install the User Instance module as you would normally install a contributed Drupal
   module. Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

 Go to /admin/config/people/custom_account_link for configuration.


MAINTAINER
----------

 - mortona2k https://www.drupal.org/u/mortona2k

